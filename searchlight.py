#!/usr/bin/env python3
"""
Autonomy and Control experiment
(C) Lieke van Lieshout and Tony Holman, W. van Ham, Radboud University Nijmegen 2021, released under GPLv3
"""
import numpy as np
import random
import time
import argparse
import csv
import os
import traceback
import math
import sys
from OpenGL import GL 
from PIL import Image
from psychopy import visual, event, core
from psychopy.hardware import joystick
from hypertextstim import HyperTextStim

import resources # instruction texts
from resources import files # list of images in random order

# stimuli, 600 6 groups: 
# 0:75, 300:375 move stimuli for even participants/follow stimuli for even yokers
# 75:150, 375:450 move stimuli for odd participants/follow stimuli for odd yokers
# 150:300, 450:600 foil stimuli
# 600 demo stimulus
# stimulus 600 is for training mode only
# move/follow is determined on basis of the odd/evenness of ppn+iTrial)
# reward in move trials is determined on basis of the odd/evenness of iTrial//2 + ppn
# reward in follow trials is determined on basis of the odd/evenness of iTrial)//2 + yoker
# randomization 
## constants
#args.ppn            # trial 0 = (0: move R-, 1: follow R-, 2: move R+, 3: follow R+, ...)
windowSize         = (800, 600)
fullScreen         = True # overrides windowSize
textureSize        = 1024 # power of 2 larger than largest window dimension
apertureSize       = 180  # (pixel) diameter of hole in noise
imageHeight        = 120  # (pixel)
messageDuration    = 20   # (s) message before each trial
pauseDuration      = 20   # (s) pause after 3 trials
joystickSpeed      = 3    # (pixel/frame) sensitivity of joystick
trialDuration      = 60   # (s) full trial before and after pause, without the pause
noiseSigma         = 3    # (full range) standard deviation of gaussian noise
joystickThresshold = 0.5  # deviation of joystick from central position 
memoryWait         = 0.2  # (s) time to show response of memorytaks
rewardAmount       = 1/30 # (EUR) reward for correct answer in rewarded trial 

indexNames = ("first", "second", "third", "fourth", "fifth", "sixth")

## initialize stuff
# command line arguments 
parser = argparse.ArgumentParser()
parser.add_argument('-p', '--ppn', type=int, help='participant number, None for demo')
parser.add_argument('-y', '--yoker', type=int, help='ppn of yoker, no yoker means no yoked trials')
parser.add_argument('-i', '--instructions', help='show instructions', action='store_true')
parser.add_argument('-s', '--screen', type=int, help='screen number 0, 1')
parser.add_argument('-d', '--debug', action='store_true', help='debug: no shuffle')
parser.add_argument('-v', '--verbose', action='store_true', help='output per frame')
args = parser.parse_args()
# defaults for command line arguments


# output files
if args.ppn is None or args.debug: # no file output in demo mode/debug mode
	experimentFile = explorationFile = memoryFile = csv.writer(open(os.devnull, 'w'))
else:
	experimentFile = csv.writer(open("data/experiment_{:04d}.csv".format(args.ppn), "x", newline=''),
			quoting=csv.QUOTE_NONNUMERIC)
	explorationFile = csv.writer(open("data/exploration_{:04d}.csv".format(args.ppn), "x", newline=''),
			quoting=csv.QUOTE_NONNUMERIC)
	explorationFile.writerow(("iMove","time","x","y"))
	memoryFile = csv.writer(open("data/memory_{:04d}.csv".format(args.ppn), "x", newline=''),
			quoting=csv.QUOTE_NONNUMERIC)
	memoryFile.writerow(("iFile", "filename", "follow", "reward", "xCorrect", "yCorrect", "likert", "x", "y"))

# input file
if args.yoker is not None:
	yokeFile = csv.reader(open("data/exploration_{:04d}.csv".format(args.yoker), "r"),
		quoting=csv.QUOTE_NONNUMERIC)
	if args.ppn is not None:
		assert args.yoker%2 != args.ppn%2, "odd/evenness of ppn and yoker must be different"

print("number of images read: {:d}".format(len(files)))

## shuffling
# block 0 even, block 0 odd, block 1 even, block 1 odd
filesIndicesExploration = (np.arange(0, 75), np.arange(75, 150), 
	np.arange(300, 375), np.arange(375, 450)) # unshuffled
# block 0, block 1
filesIndicesMemory = ((np.arange(0, 300), np.arange(300, 600))) # unshuffled

rewardList = np.zeros([12], dtype=np.bool)
if not args.debug and args.ppn is not None:
	# shuffle 
	rgPpn = random.Random(args.ppn) # initialize random number generator for move trials and memory
	rgYoker = random.Random(args.yoker) # initialize random number generator for yoke trials
	if args.ppn%2 == 0: #even
		rgPpn.shuffle(filesIndicesExploration[0])
		rgPpn.shuffle(filesIndicesExploration[2])
		rgYoker.shuffle(filesIndicesExploration[1])
		rgYoker.shuffle(filesIndicesExploration[3])
		# even ppn, move trials are even these are filled with rgPpn
		# 1 reward for move trials in block 0, 2 rewards for move trials in block 1, according to ppn
		# 2 rewards for follow trials in block 0, 1 reward for follow trials in block 1, according to yoker
		rewardList[rgPpn.sample(range(0, 6, 2), 1) + rgPpn.sample(range(6, 12, 2), 2) + 
			rgYoker.sample(range(1, 6, 2), 2) + rgYoker.sample(range(7, 12, 2), 1)] = True

	else: # ppn is odd, yoker is even
		rgPpn.shuffle(filesIndicesExploration[1])
		rgPpn.shuffle(filesIndicesExploration[3])
		rgYoker.shuffle(filesIndicesExploration[0])
		rgYoker.shuffle(filesIndicesExploration[2])
		# odd ppn, move trials are odd these are filled with rgPpn
		# 1 reward for follow trials in block 0, 2 rewards for follow trials in block 1, according to yoker
		# 2 rewards for move trials in block 0, 1 reward for move trials in block 1, according to ppn
		rewardList[rgYoker.sample(range(0, 6, 2), 1) + rgYoker.sample(range(6, 12, 2), 2) + 
			rgPpn.sample(range(1, 6, 2), 2) + rgPpn.sample(range(7, 12, 2), 1)] = True

	random.shuffle(filesIndicesMemory[0])
	random.shuffle(filesIndicesMemory[1])
print("rewardList: ", rewardList.astype(np.int))
		
# write experiment parameters
experimentFile.writerow(["ppn", args.ppn])
experimentFile.writerow(["yoker", args.yoker])
experimentFile.writerow(["files"] + files)
experimentFile.writerow(["filesIndicesExploration"]+list(filesIndicesExploration))
experimentFile.writerow(["filesIndicesMemory"]+list(filesIndicesMemory))
experimentFile.writerow(["rewardList"] + list(rewardList.astype(np.int)))
experimentFile.writerow(["followList"] + list(((1 + np.arange(12)) % 2 != 0).astype(np.int)))

# initialize window
screen = {}
if args.screen is not None:
	screen = {"screen": int(args.screen)}
win = visual.Window(windowSize, winType="pyglet", monitor="testMonitor", waitBlanking=True,
	units="pix", allowStencil=True, fullscr=fullScreen, color="white", **screen)

experimentFile.writerow(["window size"]+list(win.size))
imageDistance = min(*win.size)//5 # distance between image centers
experimentFile.writerow(["imageHeight", imageHeight])
experimentFile.writerow(["imageDistance", imageDistance])

# lens position
pos = [0,0]
# initialize joystick
joy = joystick.Joystick(0)
useJoystick = True
try:
	x = joy.getX()
except:
	useJoystick = False
experimentFile.writerow(["joystick", int(useJoystick)])

mouse = event.Mouse(False) # don't show mouse cursor

def waitKeys(stimulus, minWait=0):
	"""
	wait for either button A or a keypress
	"""
	stimulus.draw()
	win.flip()
	if minWait:
		core.wait(minWait)
		stimulus.draw()
		win.flip()
	if useJoystick:
		while not joy.getButton(0):
			stimulus.draw()
			win.flip()
		return ["return"]
	else:
		return event.waitKeys()

def updatePosition(pos):
	"""
	update lens position with joystick, or if not present with mouse 
	"""
	if useJoystick:
		x = joy.getX()
		y = -joy.getY()
		l = (x**2+y**2)**0.5
		if l>joystickThresshold: # thresshold 
			x /= l
			y /= l
			pos[0] += joystickSpeed*x
			pos[1] += joystickSpeed*y
	else:
		pos = mouse.getPos()
	return pos

def makeImage(win, fName, height):
	"""
	make single image for memory tast with better interpolation than Psychopy does
	"""
	sourceImage = Image.open(fName)
	width = round(sourceImage.size[0]*(height/sourceImage.size[0]))
	textureSize = 2**math.ceil(math.log(max(width, height), 2)) # first power of two above maximum dimension
	# resize to square texture
	sourceImage = sourceImage.transpose(Image.FLIP_TOP_BOTTOM).resize((textureSize, textureSize), Image.BICUBIC)
	imageData = np.reshape(np.array(sourceImage.getdata(), np.float32)/255, (textureSize, textureSize, 3))
	image = visual.ImageStim(win, size=(width, height), units="pix") # psychopy image
	print("single image mean: {:6.3f}, sigma: {:6.3f}, range: {:.3f}-{:.3f}".format(
		imageData.mean(), imageData.std(), imageData.min(), imageData.max()))
	image.image = imageData # note that image.image does not have the same size as image
	return image
	
def makeImages(win, fNames):
	"""
	make background and noise image out of list of 25 images based on file names
	"""
	# background image without noise
	pimage = Image.new('RGB', [min(win.size)]*2, color=(255,255,255,0)) # PIL image
	for i, fName in enumerate(fNames): # add subimages
		subImage = Image.open(fName)
		# resize
		subImage = subImage.resize((round(subImage.size[0]*(imageHeight/subImage.size[0])), imageHeight))
		pimage.paste(subImage, (imageDistance*(i%5-2)+pimage.size[0]//2 - subImage.size[0]//2, 
			imageDistance*(2-i//5) + pimage.size[1]//2 - subImage.size[1]//2))
	pimage = pimage.transpose(Image.FLIP_TOP_BOTTOM).resize((textureSize, textureSize), Image.NEAREST) # resize
	imageData = np.reshape(np.array(pimage.getdata(), np.float32)/255, (textureSize, textureSize, 3))
	image = visual.ImageStim(win, size=[min(win.size)]*2, units="pix") # psychopy image
	print("background image mean: {:6.3f}, sigma: {:6.3f}, range: {:.3f}-{:.3f}".format(
		imageData.mean(), imageData.std(), imageData.min(), imageData.max()))
	image.image = imageData# * 2 - 1

	# noise image stimulus
	noise = visual.ImageStim(win, size=[min(win.size)]*2, units="pix")
	noiseData = np.dstack([np.random.normal(0, noiseSigma, size=(textureSize, textureSize)).astype(np.float32)]*3)
	print("noise mean: {:6.3f}, sigma: {:6.3f}, range: {:.3f}-{:.3f}".format(
		noiseData.mean(), noiseData.std(), noiseData.min(), noiseData.max()))
	noise.image = (imageData + noiseData).clip(0,1)# * 2 - 1
	return (image, noise)

def pause(duration, message):
	print("pause")
	#countdown = visual.TextStim(win, color = "gray", text=duration, pos=(200, 200), alignText="right", anchorHoriz="right")
	win.flip()
	t0 = t = time.time()
	while t < t0 + duration and not event.getKeys(keyList=["escape"]):
		message.draw()
		#countdown.text = "{:.0f}".format(duration - t + t0) 
		#countdown.draw()
		win.flip()
		t = time.time()

def moveTask(image, noise, iMove):
	"""
	execute exploration task either unyoked (yokeFile=None) or yoked
	"""
	postPause = False # True if pause is in the past
	win.flip() # prevent first interval from  being much shorter and read joystick 
	t = t0 = time.time()
	while t < t0 + trialDuration + pauseDuration and not event.getKeys(keyList=["escape"]):
		# pause
		if not postPause and t-t0 > trialDuration/2:
			pause(pauseDuration, visual.TextStim(win, color="gray", text=resources.pause.format(pauseDuration)))
			postPause = True
		# noisy image
		noise.draw()
		# image without noise in aperture
		p = updatePosition(pos) # use either joystick or mouse to update position
		#print("{:6.3f}: {:4.0f}, {:4.0f}".format(t-t0, *p))
		explorationFile.writerow([iMove, t-t0, *p])

		aperture.pos = p # center aperture on mouse position
		aperture.enabled = True
		image.draw()
		aperture.enabled = False

		win.flip() #clear screen
		t = time.time()
	win.flip()

def followTask(image, noise, iMove):
	"""
	execute exploration task either unyoked (yokeFile=None) or yoked
	"""
	postPause = False # True if pause is in the past
	win.flip() # prevent first interval from being much shorter
	t0 = t = time.time()
	for i, row in enumerate(yokeFile):
		if event.getKeys(keyList=["escape"]):
			break
		# noisy image
		noise.draw()
		# image without noise in aperture
		try: # interpreting values from file as floats/ints goes in this try block
			iMoveFile, tFile, *p = row
			if iMoveFile<iMove:
				print("INFO: previous iMove")
				continue
			if iMoveFile!=iMove:
				# we went to far
				print("ERROR: wrong iMove: {:} instead of {:}".format(iMoveFile, iMove))
				break
			
			# pause
			if not postPause and float(tFile) > trialDuration/2:
				pause(pauseDuration, visual.TextStim(win, color = "gray", text=resources.pause.format(pauseDuration)))
				postPause = True
			aperture.pos = p # center aperture on mouse position
		except:
			print("bad line:", row)
			continue # ignore bad lines in yokeFile (including header)
		
		aperture.enabled = True
		image.draw()
		aperture.enabled = False

		win.flip() # clear screen
		t = time.time()
		if args.verbose:
			try:
				print("{:3d}: tFile: {:.6f}, t: {:.6f}, delta t: {:.6f}, ".format(i, tFile, t-t0, t-t0-tFile))
			except:
				pass
	win.flip() #clear screen
	try:
		print("number of lines read in yokefile: {:d}".format(i))
	except:
		# file ended, usually on second call with yoke file that has no second iMove
		print(traceback.format_exc()) 

def getDirection():
	"""
	direction of response, from keyboard or joystick, non blocking, like getKeys
	0: no response yet
	-1: end of trial
	"""
	if(event.getKeys(keyList=["escape"])):
		print("gracious abort")
		#sys.exit()
		raise NameError('UserAbort')
	if useJoystick:
		if joy.getButton(0):
			return ["return"]
		x = joy.getX()
		y = -joy.getY()
		l = (x**2+y**2)**0.5
		if l>joystickThresshold:
			if x<0 and abs(x)>abs(y):
				return ["left"]
			elif y<0 and abs(y)>abs(x):
				return ["down"]
			elif y>0 and abs(y)>abs(x):
				return ["up"]
			elif x>0 and abs(x)>abs(y):
				return ["right"]
		return([])
	else:
		return event.getKeys()

def likertResponse():
	"""
	position on likert scale (1-4) participant is pointing to or 0 for no choice yet
	"""
	keys = getDirection()
	if "left" in keys:
		return 1 # Definitely OLD
	elif "down" in keys:
		return 2 # Probably OLD
	elif "up" in keys:
		return 3 # Probably NEW
	elif "right" in keys:
		return 4 # Definitely NEW
	return 0

def memoryTask(iFile, fName, follow, reward, xi, yi):
	"""
	follow/reward: -1 foil stimulus, 0: False, 1: True
	xiCorrect, yiCorrect: -2 -- 2 =position, -5: foil stimulus
	iFile: index in files array
	"""
	likert = [
		visual.TextStim(win, color = "gray", text="Definitely OLD", pos=(-300, 0)),
		visual.TextStim(win, color = "gray", text="Probably OLD", pos=(0, -200)),
		visual.TextStim(win, color = "gray", text="Probably NEW", pos=(0, 200)),
		visual.TextStim(win, color = "gray", text="Definitely NEW", pos=(300, 0))]
	question = visual.TextStim(win, color = "gray", text='Do you recognize this image?', pos=(0, 250))
	#image = visual.ImageStim(win, fName, units="pixels", size=(imageHeight, imageHeight), interpolate=True)
	image = makeImage(win, fName, imageHeight)
	placeholder = [visual.Circle(win, 
		units="height", radius=0.1, pos=(0.2*(i//5-2), 0.2*(i%5-2)), fillColor=(.9, .9, .9)) for i in range(25)]
	
	# wait for joystick return to center and empty key buffer
	while likertResponse():
		win.flip()

	# wait for response with joystick or keyboard
	r=0
	while r==0:
		question.draw()
		image.draw()
		# draw answer options
		for stimulus in likert:
			stimulus.draw()
		win.flip()
		r = likertResponse()
	rLikert = r
		
	# draw given response
	for i, stimulus in enumerate(likert):
		if i+1 == r:
			stimulus.color = "blue"
		stimulus.draw()
		stimulus.color = "gray"
	win.flip()
	core.wait(memoryWait)

	# position image on grid
	rxi, ryi = (0,0)
	score = False
	if rLikert in (1,2):
		question.text="Put the image at the correct position and press the triggerbutton"
		while True:
			for stimulus in placeholder:
				stimulus.draw()
			question.draw()
			image.draw() # positioned image
			win.flip()
			r = getDirection()
			if r:
				if "return" in r:
					break
				x, y = image.pos
				if "right" in r and -2 <= rxi <=1:
					rxi += 1
				elif "left" in r and -1 <= rxi <=2:
					rxi -= 1
				elif "up" in r  and -2 <= ryi <=1:
					ryi += 1
				elif "down" in r and -1 <= ryi <=2:
					ryi -= 1
				image.pos = (rxi*imageDistance, ryi*imageDistance)
				
				# wait for joystick return to center
				while getDirection():
					for stimulus in placeholder:
						stimulus.draw()
					question.draw()
					image.draw() # positioned image
					win.flip() 

	# write to file
	print("memory of {:3d}: {:s}, f:{:d}, r:{:d}, correct location: {:d}, {:d}, response {:d}: ({:d}, {:d})".format(
		iFile, fName, follow, reward, xi, yi, rLikert, rxi, ryi))
	memoryFile.writerow([iFile, fName, follow, reward, xi, yi, rLikert, rxi, ryi])
	
	#if rLikert in (1,2) and xi == rxi and yi == ryi:
	if rLikert in (1,2) and (xi, yi) != (-5, -5):
		return True
	return False # foil or not recognized


# aperture for use with image without noisea
aperture = visual.Aperture(win, size=apertureSize, shape="circle")
aperture.enabled = False

# demo mode
if args.ppn is None:
# instructions
	if args.instructions:
		waitKeys(HyperTextStim(win, text=resources.instructionsDemo0, font="FreeSerif", color="gray"), minWait=2)
		waitKeys(HyperTextStim(win, text=resources.instructionsDemo1, font="FreeSerif", color="gray"), minWait=2)
		waitKeys(HyperTextStim(win, text=resources.instructionsDemo2, font="FreeSerif", color="gray"), minWait=2)

	files = ["stimuli_dummy/rossion/{:03d}.png".format(i+1) for i in range(100)]
	condition = [{"follow": -1, "reward": -1, "xi":-5, "yi":-5}]*len(files)  # default value, becomes 0/1
	for iTrial in range(4):
		image, noise = makeImages(win, files[25*iTrial:25*(iTrial+1)])

		iMove = iTrial // 2 # 0-1, perhaps iPair is a better name
		follow = iTrial%2
		reward = iTrial//2
		# write condition array for use in memory task
		
		for i in range(iTrial*25, (iTrial+1)*25):
			condition[i] = {"follow": int(follow), "reward": int(reward), "xi": i%5-2, "yi": i//5-2}

		# pre trial instructions
		text1 = "demo block, {:s} trial\n".format(indexNames[iTrial])
		text1 += resources.preTrialDemo[iTrial]
		if iTrial != 3:
			waitKeys(HyperTextStim(win, text=text1, color="gray"), minWait=2)
		else: # before last trial
			pause(messageDuration, HyperTextStim(win, text=text1, color="gray"))

		if not follow: # move trial
			#waitKeys(visual.TextStim(win, text+resources.moveDemo, color="gray"))
			moveTask(image, noise, 0)
		else: # follow trial
			if args.yoker is not None:
				#waitKeys(visual.TextStim(win, text+resources.followDemo, color="gray"))
				followTask(image, noise, iMove)

	# memory task
	indices = list(range(100))
	random.shuffle(indices)
	waitKeys(visual.TextStim(win, resources.memoryDemo, color="gray"))
	for iFile in indices[0:20]:
		try:
			score = memoryTask(iFile, files[iFile], **condition[iFile])
		except NameError:
			print('user aborted memory task')
			break
	del aperture, image, noise
	quit()
	
# instructions
if args.instructions:
	waitKeys(HyperTextStim(win, text=resources.instructions0, font="FreeSerif", color="gray"), minWait=2)
	waitKeys(HyperTextStim(win, text=resources.instructions1, font="FreeSerif", color="gray"), minWait=2)
	waitKeys(HyperTextStim(win, text=resources.instructions2, font="FreeSerif", color="gray"), minWait=2)

# experiment mode
memoryScore = [0, 0] # counter of scores in memory task
rewardedScore = 0
condition = [{"follow": -1, "reward": -1, "xi":-5, "yi":-5}]*600  # default value, becomes 0/1
for iBlock in range(2):
	for iTrial in range(6):
		## determine conditions: move and reward
		# iAllTrial is the real trial counter for cou nterbalancing, blocks do not influence the conditioning
		iAllTrial = iTrial + 6*iBlock # 0-11
		# iMove is the counter of the move trials for whole experiment, always 0-5 in file
		iMove = iAllTrial // 2 # 0-5, perhaps iPair is a better name
		follow = (args.ppn + iAllTrial) % 2 != 0 # is this a follow trial 1, 3, 5, ...
		# skip yoke trials if no yoker is given, necessary for first participant
		if follow and args.yoker is None: # skip yoked trials if there is no yoker
			continue
		reward = rewardList[iAllTrial]
		print("iAllTrial: {:d}, follow: {:d}, reward: {:d}".format(iAllTrial, follow, reward))
		
		# choose stimuli
		odd = follow and args.yoker%2 or follow==False and args.ppn%2
		start = (iMove%3)*25
		indices = filesIndicesExploration[2*iBlock+odd][start:start+25] # array of 25 indices in files array
		image, noise = makeImages(win, [files[index] for index in indices])
		
		# write condition array for use in memory task
		for i, iFile in enumerate(indices):
			condition[iFile] = {"follow": int(follow), "reward": int(reward), "xi": i%5-2, "yi": i//5-2}
			
		print("iFile: "+",".join([str(i) for i in indices]))
		
		## execute trial
		# pre trial instructions
		text1 = "{:s} block, {:s} trial\n".format(indexNames[iBlock], indexNames[iTrial])
		text1 += resources.preTrial[2*reward+follow]
		pause(messageDuration, HyperTextStim(win, text=text1, color="gray"))
		
		# trial
		if not follow:
			moveTask(image, noise, iMove)
		else:
			followTask(image, noise, iMove)

	# memory task
	indices = filesIndicesMemory[iBlock] # 0:300 shuffled indices in files array
	waitKeys(visual.TextStim(win, resources.memory, color="gray"))
	for iFile in indices:
		try:
			score = memoryTask(iFile, files[iFile], **condition[iFile])
		except NameError:
			print('user aborted memory task')
			break
		if score:
			memoryScore[iBlock] += 1
			if condition[iFile]["reward"]==1:
				rewardedScore += 1
	if iBlock == 0:
		waitKeys(HyperTextStim(win, text=resources.midBlock, color="gray"))

experimentFile.writerow(["memoryScore[0]", memoryScore[0]])
experimentFile.writerow(["memoryScore[1]", memoryScore[1]])
experimentFile.writerow(["rewardedScore", rewardedScore])
experimentFile.writerow(["rewardedAmount", rewardedScore*rewardAmount])

text = resources.final.format(*memoryScore, rewardedScore, rewardedScore*rewardAmount)
waitKeys(visual.TextStim(win, text, color="gray"))
del aperture
