#!/usr/bin/env python3
import numpy as np
import random

rewardListList = []
for ppn in range(8):
	yoker = ppn -1
	filesIndicesExploration = (np.arange(0, 75), np.arange(75, 150), 	np.arange(300, 375), np.arange(375, 450))
	filesIndicesMemory = ((np.arange(0, 300), np.arange(300, 600))) # unshuffled
	rgPpn = random.Random(ppn) # initialize random number generator for move trials and memory
	rgYoker = random.Random(yoker) # initialize random number generator for yoke trials
	rgPpn.shuffle(filesIndicesExploration[0])
	rgPpn.shuffle(filesIndicesExploration[2])
	rgYoker.shuffle(filesIndicesExploration[1])
	rgYoker.shuffle(filesIndicesExploration[3])
	random.shuffle(filesIndicesMemory[0])
	random.shuffle(filesIndicesMemory[1])
	rewardList = np.zeros([12], dtype=np.bool)
	if ppn%2 == 0:
		# even ppn, move trials are even these are filled with rgPpn
		# 1 reward for move trials in block 0, 2 rewards for move trials in block 1, according to ppn
		# 2 rewards for follow trials in block 0, 1 reward for follow trials in block 1, according to yoker
		rewardList[ rgPpn.sample(range(0, 6, 2), 1) + rgPpn.sample(range(6, 12, 2), 2) + 
			rgYoker.sample(range(1, 6, 2), 2) + rgYoker.sample(range(7, 12, 2), 1)] = True
	else: # ppn is odd, yoker is even
		# odd ppn, move trials are odd these are filled with rgPpn
		# 1 reward for follow trials in block 0, 2 rewards for follow trials in block 1, according to yoker
		# 2 rewards for move trials in block 0, 1 reward for move trials in block 1, according to ppn
		rewardList[rgYoker.sample(range(0, 6, 2), 1) + rgYoker.sample(range(6, 12, 2), 2) + 
			rgPpn.sample(range(1, 6, 2), 2) + rgPpn.sample(range(7, 12, 2), 1)] = True
	rewardListList.append(rewardList.astype(np.int))

# iTrial iMove follow reward
print("iTrial iPair"+" ".join(["  F{:d}R ".format(i) for i in range(5)]))
for i in range(12):
	iMove = i//2
	print("    {:2d}     {:d} ".format(i, iMove), end="")
	if(i%2==0):
		print(" {:b} {:b}".format(i%2, rewardListList[0][i]), end="") # move
	else:
		print(" "*4, end="") # no follow trial for ppn=0
	for ppn in range(1,5):
		print("{:s}{:b} {:b}".format(("    ", " <- ")[(i+ppn)%2], (i+ppn)%2, rewardListList[ppn][i]), end="") # move
	print()
